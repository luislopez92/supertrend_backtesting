import yfinance as yf
import pandas as pd

# 1. Import Yahoo Finance stock market data for TSLA in the 1 day time frame
ticker = 'TSLA'
start_date = '2020-01-01'
end_date = '2023-12-31'

data = yf.download(ticker, start=start_date, end=end_date, interval='1d')
data = data.dropna()

# Calculate Heikin Ashi data
def heikin_ashi(data):
    ha_data = pd.DataFrame(index=data.index)
    ha_data['Close'] = (data.Open + data.High + data.Low + data.Close) / 4
    ha_data['Open'] = (data.Open.shift() + data.Close.shift()) / 2
    ha_data.Open.iloc[0] = (data.Open[0] + data.Close[0]) / 2
    ha_data['High'] = data[['High', 'Open', 'Close']].max(axis=1)
    ha_data['Low'] = data[['Low', 'Open', 'Close']].min(axis=1)
    return ha_data


    
