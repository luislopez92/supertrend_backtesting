from candlestick_plot_method import plot_supertrend_ha
from import_data import import_data
from parameter_optimization import find_optimal_parameter
from heikin_ashi_data import heikin_ashi
from stock_screener import stock_screener
from backtrader_supertrend_v2 import backtrader_supertrend
from bt_parameter_optimization import find_optimal_bt_parameter
from optimize_ticker import optimize_tickers

# Get top tickers list from the stock screener method
tickers = stock_screener()

# Get the #1 ticker in the list 
top_symbol = str(tickers[0])

# Parameters
start = '2023-03-01'
end = '2023-03-30'
interval = '30m'
investment=5000
comission=0

# Find the best parameters for the best tickers
optimize_tickers(tickers, start, end, interval, investment, comission)

# Run the parameter optimiziation method to find best parameters
best_parameters = find_optimal_bt_parameter(top_symbol, start, end, interval, investment, comission)
print("Best parameters:", best_parameters)


# These are the optimal parameters
optimal_mult = best_parameters[1]
optimal_period = best_parameters[0]


# Backtrader test with supertrend 
backtrader_supertrend(top_symbol, start, end, interval, investment,comission, period=optimal_period,multiplier=optimal_mult)

# import data, convert it to HA, and plot to compare to backtest
df = import_data(top_symbol,start,end,interval)
df = heikin_ashi(df)
plot_supertrend_ha(df,optimal_period,optimal_mult)












