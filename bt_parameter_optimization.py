from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import backtrader as bt
import pandas as pd
import yfinance as yf

class SuperTrend(bt.Indicator):
    
    def __init__(self, period=None, multiplier=None):
        if period is not None:
            self.p.period = period
        if multiplier is not None:
            self.p.multiplier = multiplier

        self.st = [0]
        self.finalupband = [0]
        self.finallowband = [0]
        self.addminperiod(self.p.period)
        atr = bt.ind.ATR(self.data, period=self.p.period)
        self.upperband = (self.data.high + self.data.low) / 2 + self.p.multiplier * atr
        self.lowerband = (self.data.high + self.data.low) / 2 - self.p.multiplier * atr
        
    lines = ('super_trend',)
    plotlines = dict(
        super_trend=dict(
            _name='ST',
            color='blue',
            alpha=1
        )
    )
    
    plotinfo = dict(subplot=False)


    def next(self):
        pre_upband = self.finalupband[0]
        pre_lowband = self.finallowband[0]
        if self.upperband[0] < self.finalupband[-1] or self.data.close[-1] > self.finalupband[-1]:
            self.finalupband[0] = self.upperband[0]
        else:
            self.finalupband[0] = self.finalupband[-1]
        if self.lowerband[0] > self.finallowband[-1] or self.data.close[-1] < self.finallowband[-1]:
            self.finallowband[0] = self.lowerband[0]
        else:
            self.finallowband[0] = self.finallowband[-1]
        if self.data.close[0] <= self.finalupband[0] and ((self.st[-1] == pre_upband)):
            self.st[0] = self.finalupband[0]
            self.lines.super_trend[0] = self.finalupband[0]
        elif (self.st[-1] == pre_upband) and (self.data.close[0] > self.finalupband[0]):
            self.st[0] = self.finallowband[0]
            self.lines.super_trend[0] = self.finallowband[0]
        elif (self.st[-1] == pre_lowband) and (self.data.close[0] >= self.finallowband[0]):
            self.st[0] = self.finallowband[0]
            self.lines.super_trend[0] = self.finallowband[0]
        elif (self.st[-1] == pre_lowband) and (self.data.close[0] < self.finallowband[0]):
            self.st[0] = self.finalupband[0]
            self.lines.super_trend[0] = self.st[0]

class testStrategy(bt.Strategy):

    def log(self, txt, dt=None):
        if  True:
            dt = dt or self.datas[0].datetime.date(0)
            print('%s - %s' % (dt.isoformat(), txt))
 
    def __init__(self, period=None, multiplier=None):
        self.x = SuperTrend(self.data, period=period, multiplier=multiplier)
        self.dclose = self.datas[0].close
        self.cross = bt.ind.CrossOver(self.dclose, self.x)

    # def notify(self, order):
    #     if order.status in [order.Submitted, order.Accepted]:
    #         # Buy/Sell order submitted/accepted to/by broker - Nothing to do
    #         return
    #     # Check if an order has been completed
    #     # Attention: broker could reject order if not enougth cash
    #     if order.status in [order.Completed, order.Canceled, order.Margin]:
    #         if order.isbuy():
    #             self.log(
    #                 'BUY EXECUTED: %s, Price: %.2f, Cost: %.2f, Comm %.2f' %
    #                 (order.data._name,
    #                  order.executed.price,
    #                  order.executed.value,
    #                  order.executed.comm))
    #             self.buyprice = order.executed.price
    #             self.buycomm = order.executed.comm
    #             self.opsize = order.executed.size
    #         else:  # Sell
    #             self.log('SELL EXECUTED: %s, Price: %.2f, Cost: %.2f, Comm: %.2f' %
    #                      (order.data._name,
    #                       order.executed.price,
    #                       order.executed.value,
    #                       order.executed.comm))
    #     elif order.status in [order.Canceled, order.Margin, order.Rejected]:
    #         self.log('Order Canceled/Margin/Rejected')
    #     self.order = None 
                
    # def notify_trade(self, trade):
    #     if trade.isclosed:
    #         self.log('TRADE PROFIT: EQ %s, GROSS %.2f, NET %.2f' %
    #                  ('Closed'  , trade.pnl, trade.pnlcomm))
    #     elif trade.justopened:
    #         self.log('TRADE OPENED: EQ %s, SIZE %2d' % (  'Opened'  , trade.size))
                
    def next(self):
        pos = self.getposition(self.data)
        dpos = pos.size
        if self.cross[0]==1 and dpos <= 0:
            self.order_target_percent(data=self.data, target=0.5)
        elif self.cross[0]==-1 and dpos >= 0:
            self.order_target_percent(data=self.data, target=-0.5)

def find_optimal_bt_parameter(symbol, start, end, interval, investment, comission):
    # predefine several parameter sets
    atr_period = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14]
    atr_multiplier = [1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]
    roi_list = []

    # for each period and multiplier, perform backtest
    for period, multiplier in [(x, y) for x in atr_period for y in atr_multiplier]:
        initial_value, final_value = backtrader_supertrend(symbol, start, end, interval, investment, comission, period, multiplier)
        roi = round(((final_value - initial_value) / (initial_value)) * 100, 2)
        roi_list.append((period, multiplier, roi))

    print(pd.DataFrame(roi_list, columns=['ATR_period', 'Multiplier', 'ROI']))

    # return the best parameter set
    return max(roi_list, key=lambda x: x[2])



def backtrader_supertrend(symbol, start, end, interval, investment, comission, period, multiplier):
    cerebro = bt.Cerebro()
    data = bt.feeds.PandasData(dataname=yf.download(symbol, start, end, interval=interval,auto_adjust=True))
    data.addfilter(bt.filters.HeikinAshi)
    cerebro.adddata(data)
    cerebro.broker.setcash(investment)

    cerebro.broker.setcommission(commission=comission)

    # #Add our strategy
    cerebro.addstrategy(testStrategy, period=period, multiplier=multiplier)

    initial_value = cerebro.broker.getvalue()
    result = cerebro.run()
    final_value = cerebro.broker.getvalue()
    roi= round(((final_value-initial_value)/(initial_value))*100,2)
    return initial_value, final_value


# Call the new function to optimize the parameters
# best_parameters = find_optimal_bt_parameter('TSLA', '2023-03-01', '2023-04-01', '30m', 5000, 0)
# print("Best parameters:", best_parameters)