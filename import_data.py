import yfinance as yf

def import_data(symbol,start,end,interval):
    symbol = symbol
    data = yf.download(symbol, start=start, end=end,interval=interval)
    data = data.dropna()
    

    return data