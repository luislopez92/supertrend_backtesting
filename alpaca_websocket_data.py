import pandas as pd
from datetime import datetime
from alpaca.data.live import StockDataStream

# Alpaca API keys
API_KEY ='PKA1IYK7LZC2ZLVXD07H'
SECRET_KEY = 'IRle393smy6mSm9WXFaeIpik5JPTEtDtYkXf68dt'

# Initialize an empty DataFrame with the desired columns
live_data = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])

import pandas as pd

def supertrend(data, period, multiplier):
    data = data.copy()
    hl2 = (data['high'] + data['low']) / 2
    atr = data['high'].rolling(window=period).max() - data['low'].rolling(window=period).min()
    basic_upperband = hl2 + multiplier * atr
    basic_lowerband = hl2 - multiplier * atr

    final_upperband = basic_upperband.copy()
    final_lowerband = basic_lowerband.copy()

    for i in range(1, len(data)):
        if basic_upperband[i] < final_upperband[i - 1] or data['close'][i - 1] > final_upperband[i - 1]:
            final_upperband.loc[i] = basic_upperband[i]
        else:
            final_upperband.loc[i] = final_upperband[i - 1]

        if basic_lowerband[i] > final_lowerband[i - 1] or data['close'][i - 1] < final_lowerband[i - 1]:
            final_lowerband.loc[i] = basic_lowerband[i]
        else:
            final_lowerband.loc[i] = final_lowerband[i - 1]

    st = pd.Series(float('nan'), index=data.index)

    # Initialize the first non-NaN value
    st[period] = final_upperband[period]

    for i in range(period + 1, len(data)):
        pre_upband = final_upperband[i - 1]
        pre_lowband = final_lowerband[i - 1]

        if data['close'][i] <= final_upperband[i] and (st[i - 1] == pre_upband):
            st.loc[i] = final_upperband[i]
        elif (st[i - 1] == pre_upband) and (data['close'][i] > final_upperband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] >= final_lowerband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] < final_lowerband[i]):
            st.loc[i] = final_upperband[i]


    supertrend_line = st

    return final_upperband, final_lowerband, supertrend_line


async def quote_data_handler(data):
    global live_data
    # Append the new data to the DataFrame
    live_data = live_data.append({
        'timestamp': data.timestamp.replace(tzinfo=None),
        'open': data.open,
        'high': data.high,
        'low': data.low,
        'close': data.close,
        'volume': data.volume
    }, ignore_index=True)
    
    # Calculate final_upperband and final_lowerband
    period = 1
    multiplier = 1
    final_upperband, final_lowerband, supertrend_line = supertrend(live_data, period, multiplier)
    
    # Add columns to the live_data DataFrame
    live_data['final_upperband'] = final_upperband
    live_data['final_lowerband'] = final_lowerband
    live_data['supertrend_line'] = supertrend_line

    print(live_data.tail(5))  # Print the latest row

wss_client = StockDataStream(API_KEY, SECRET_KEY)
wss_client.subscribe_bars(quote_data_handler, 'TSLA')

wss_client.run()
