// Wait for the DOM to be ready
document.addEventListener("DOMContentLoaded", function () {
  Highcharts.getJSON("./data/data.json", function (data) {
    Highcharts.stockChart("container", {
      rangeSelector: {
        selected: 1,
      },

      title: {
        text: "SUPERTRADIER 💰 ",
      },
      navigator: {
        series: {
          color: 'blue',
          fillColor: 'rgba(0, 0, 255, 0.1)'
        }
      },
      series: [
        {
          id: "stock",
          type: "heikinashi",
          name: "Stock Data",
          data: data,
          color:'crimson', // bearish candles
          upColor: "lightgreen", // bullish candles
          dataGrouping: {
            enabled: false,
          },
        },
        {
          type: "supertrend",
          linkedTo: "stock",
          params: {
            multiplier: 3,
            period: 10,
          },
          lineWidth: 2,
          marker: {
            enabled: false,
          },
          risingTrendColor: "#16C535",
          fallingTrendColor: "#F22303",
          changeTrendLine: {
            styles: {
              lineWidth: 0.5,
              lineColor: "#000",
              dashStyle: "Dash",
            },
          },
        },
      ],
    });
  });
});
