from datetime import datetime, timedelta
import yfinance as yf
import os
import json

def BackTester(symbol, start_date, end_date, timeframe):
    df = yf.download(symbol, start=start_date, end=end_date, interval=timeframe)
    # Create folder if it doesn't exist
    folder_name = 'data'
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)

    # Convert data to OHLC format
    ohlc_data = []
    for index, row in df.iterrows():
        timestamp = int(index.timestamp()) * 1000
        ohlc_data.append([
            timestamp,
            float(row['Open']),
            float(row['High']),
            float(row['Low']),
            float(row['Close'])
        ])

    # Save data as JSON file
    file_name = os.path.join(folder_name, 'data.json')
    with open(file_name, 'w') as f:
        json.dump(ohlc_data, f)

#last x number of days. using 60 as example
start_date = datetime.now() - timedelta(days=60)
end_date = datetime.now()

BackTester('TSLA', start_date, end_date, '30m')