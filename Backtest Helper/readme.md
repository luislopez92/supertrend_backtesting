# Follow these steps to use the backtest

1. open the backtester.py file and set your parameters `symbol` and `date_start` and `date_end` and `timeframe`
    a. make sure to change directories to the Backtest Helper folder.
2. open dashboard.html using 'LiveServer', the screen will be blank.
3. Run the following command. this command will generate a data.json file.
```
python '.\Backtest Helper\backtester.py'
```

## the data will appear on the screen.