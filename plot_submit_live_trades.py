from dash import Dash, dcc, html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import numpy as np
import pandas_ta as ta
import requests
from datetime import datetime
from alpaca.data.live import StockDataStream
import alpaca_trade_api as tradeapi
import json
import time
import threading
from flask import Flask
import os

app = Dash(__name__)
server = app.server


# Alpaca API keys
API_KEY ='PKKBLWOBXR8MGKFPDTOR'
SECRET_KEY = 'sheybuExwJj7PeUZMzfxcEgJbAyX8sgbcJmWUqF3'
HEADERS = {'APCA-API-KEY-ID': API_KEY, 'APCA-API-SECRET-KEY': SECRET_KEY}

BASE_URL = "https://paper-api.alpaca.markets"
POSITIONS_URL = "{}/v2/positions".format(BASE_URL)

# Calculate Heikin Ashi data
def heikin_ashi(data):
    ha_data = pd.DataFrame(index=data.index)
    ha_data['timestamp'] = data.timestamp #added this
    ha_data['close'] = (data.open + data.high + data.low + data.close) / 4
    ha_data['open'] = (data.open.shift() + data.close.shift()) / 2

    if not data.empty:
        ha_data.open.iloc[0] = (data.open[0] + data.close[0]) / 2

    ha_data['high'] = data[['high', 'open', 'close']].max(axis=1)
    ha_data['low'] = data[['low', 'open', 'close']].min(axis=1)

    return ha_data

# Initialize an empty DataFrame with the desired columns
live_data = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])
live_data_ha = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])

# Load the data from the CSV file if it exists
if os.path.exists('live_data_ha.csv'):
    live_data_ha = pd.read_csv('live_data_ha.csv')


# Initialize the Alpaca API connection
api = tradeapi.REST(API_KEY, SECRET_KEY, base_url='https://paper-api.alpaca.markets', api_version='v2')

def delete_positions(symbol):
	data = {
		"symbol": symbol
	}

	r = requests.delete(POSITIONS_URL+'/'+symbol, headers=HEADERS)

	return json.loads(r.content)

# Initialize the websocket client
wss_client = StockDataStream(API_KEY, SECRET_KEY)

def run_wss_client():
    wss_client.subscribe_bars(quote_data_handler, 'TSLA')
    wss_client.run()

@server.before_first_request
def run_wss_on_startup():
    wss_thread = threading.Thread(target=run_wss_client)
    wss_thread.start()


def supertrend(data, period, multiplier):
    data = data.copy()
    hl2 = (data['high'] + data['low']) / 2
    atr = data['high'].rolling(window=period).max() - data['low'].rolling(window=period).min()
    basic_upperband = hl2 + multiplier * atr
    basic_lowerband = hl2 - multiplier * atr

    final_upperband = basic_upperband.copy()
    final_lowerband = basic_lowerband.copy()

    for i in range(1, len(data)):
        if basic_upperband[i] < final_upperband[i - 1] or data['close'][i - 1] > final_upperband[i - 1]:
            final_upperband.loc[i] = basic_upperband[i]
        else:
            final_upperband.loc[i] = final_upperband[i - 1]

        if basic_lowerband[i] > final_lowerband[i - 1] or data['close'][i - 1] < final_lowerband[i - 1]:
            final_lowerband.loc[i] = basic_lowerband[i]
        else:
            final_lowerband.loc[i] = final_lowerband[i - 1]

    st = pd.Series(float('nan'), index=data.index)

    # Initialize the first non-NaN value
    st.iloc[period] = final_upperband.iloc[period]

    for i in range(period + 1, len(data)):
        pre_upband = final_upperband[i - 1]
        pre_lowband = final_lowerband[i - 1]

        if data['close'][i] <= final_upperband[i] and (st[i - 1] == pre_upband):
            st.loc[i] = final_upperband[i]
        elif (st[i - 1] == pre_upband) and (data['close'][i] > final_upperband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] >= final_lowerband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] < final_lowerband[i]):
            st.loc[i] = final_upperband[i]


    supertrend_line = st

    return final_upperband, final_lowerband, supertrend_line


async def quote_data_handler(data):
    global live_data
    global live_data_ha
    

    # Append the new data to the DataFrame
    live_data = live_data.append({
        'timestamp': data.timestamp.replace(tzinfo=None),
        'open': data.open,
        'high': data.high,
        'low': data.low,
        'close': data.close,
        'volume': data.volume
    }, ignore_index=True)

    # Calculate Heikin Ashi data
    live_data_ha = heikin_ashi(live_data)


    # Initialize the variables before the condition
    final_upperband = None
    final_lowerband = None
    supertrend_line = None
    
    # Calculate final_upperband and final_lowerband
    period = 1
    multiplier = 1

    if len(live_data_ha) > period + 1:
        final_upperband, final_lowerband, supertrend_line = supertrend(live_data_ha, period, multiplier)
    
    # Add columns to the live_data DataFrame
    live_data_ha['final_upperband'] = final_upperband
    live_data_ha['final_lowerband'] = final_lowerband
    live_data_ha['supertrend_line'] = supertrend_line

    print(live_data_ha)  # Print the latest row

    # Save the live_data_ha DataFrame to a CSV file
    live_data_ha.to_csv('live_data_ha.csv', index=False)

    # Trade logic
    if not live_data_ha.empty and len(live_data_ha) > period + 1:
        if live_data_ha['close'].iloc[-1] > supertrend_line.iloc[-1] and live_data_ha['close'].iloc[-2] <= supertrend_line.iloc[-2]:
            # Buy signal
            print("Buy signal")

            # Cancel all open orders
            delete_positions("TSLA")
            print('Previous Position Closed')

            time.sleep(5)

            latest_price = api.get_latest_trade('TSLA')

            #slippage = abs(latest_price.p-live_data_ha['close'][0])
            slippage = abs(latest_price.p-live_data_ha['close'].iloc[-1])

            slippage_ticks = round(slippage*100,2)
            
            # Submit buy order
            account = api.get_account()
            buying_power = float(account.buying_power)
            target_percent = 0.1  # Adjust this value to your desired target percentage
            target_quantity = int((buying_power * target_percent) / live_data_ha['close'].iloc[-1])
            
            if target_quantity > 0: #don't really need this tbh
                if slippage < 1:
                    api.submit_order(
                        symbol='TSLA',
                        qty=target_quantity,
                        side='buy',
                        type='market',
                        time_in_force='gtc'
                    )
                    print("Buy order submitted")
                    print("slippage (ticks) is: "+str(slippage_ticks))
                else:
                    print("Slippage (ticks) is: "+str(slippage_ticks))
                    pass

        elif live_data_ha['close'].iloc[-1] < supertrend_line.iloc[-1] and live_data_ha['close'].iloc[-2] >= supertrend_line.iloc[-2]:
            # Sell signal
            print("Sell signal")

            # Cancel all open orders
            delete_positions("TSLA")
            print('Previous Position Closed')

            time.sleep(5)

            latest_price = api.get_latest_trade('TSLA')

            # slippage = abs(latest_price.p-live_data_ha['close'][0])
            slippage = abs(latest_price.p-live_data_ha['close'].iloc[-1])

            slippage_ticks = round(slippage*100,2)

            account = api.get_account()
            buying_power = float(account.buying_power)
            target_percent = 0.1  # Adjust this value to your desired target percentage
            target_quantity = int((buying_power * target_percent) / live_data_ha['close'].iloc[-1])
            abs_target_quantity = abs(target_quantity)

            if target_quantity > 0: #don't really need this tbh
                if slippage < 1:
                    api.submit_order(
                        symbol='TSLA',
                        qty=target_quantity,
                        side='sell',
                        type='market',
                        time_in_force='gtc'
                    )
                    print("Sell order submitted")
                    print("slippage (ticks) is: "+str(slippage_ticks))
                else:
                    print("Slippage (ticks) is: "+str(slippage_ticks))
                    pass

#Try to plot
app.layout = html.Div([

    html.H1(id="count-up"),
    dcc.Graph(id="candles"),
    dcc.Graph(id="indicator"),
    dcc.Interval(id="interval",interval=2000), 

    ])

@app.callback(
    [Output("candles", "figure"), Output("indicator", "figure")],
    Input("interval", "n_intervals"),
)



def update_figure(n_intervals):
    global live_data_ha

    data = live_data_ha.copy()
    data["rsi"] = ta.rsi(data.close.astype(float))

    # Create buy and sell signals lists
    buy_signals = []
    sell_signals = []

    for i in range(2, len(data)):
        if data['close'].iloc[i] > data['supertrend_line'].iloc[i] and data['close'].iloc[i - 1] <= data['supertrend_line'].iloc[i - 1]:
            buy_signals.append((data.timestamp.iloc[i], data['supertrend_line'].iloc[i]))
        elif data['close'].iloc[i] < data['supertrend_line'].iloc[i] and data['close'].iloc[i - 1] >= data['supertrend_line'].iloc[i - 1]:
            sell_signals.append((data.timestamp.iloc[i], data['supertrend_line'].iloc[i]))

    buy_signals_x, buy_signals_y = zip(*buy_signals) if buy_signals else ([], [])
    sell_signals_x, sell_signals_y = zip(*sell_signals) if sell_signals else ([], [])

    # Create scatter plot for buy signals
    buy_scatter = go.Scatter(
        x=buy_signals_x,
        y=[y - (data.high.max() - data.low.min()) * 0.05 for y in buy_signals_y],  # Adjust the position of the triangle

        mode='markers',
        marker=dict(
            symbol='triangle-up',  # Triangle marker
            size=16,
            color='green',
            line=dict(width=1)
        ),
        name='Buy Signal'
    )

    # Create scatter plot for buy signal texts
    buy_text_scatter = go.Scatter(
        x=buy_signals_x,
        y=[y - (data.high.max() - data.low.min()) * 0.15 for y in buy_signals_y],  # Adjust the position of the text
        mode='text',
        text='Buy Signal',
        textposition='middle center',
        textfont=dict(color='green'),
        showlegend=False
    )

    # Create scatter plot for buy signals
    sell_scatter = go.Scatter(
        x=sell_signals_x,
        y=[y + (data.high.max() - data.low.min()) * 0.05 for y in sell_signals_y],  # Adjust the position of the triangle
        mode='markers',
        marker=dict(
            symbol='triangle-down',  # Triangle marker
            size=16,
            color='red',
            line=dict(width=1)
        ),
        name='Sell Signal'
    )

    # Create scatter plot for buy signal texts
    sell_text_scatter = go.Scatter(
        x=sell_signals_x,
        y=[y + (data.high.max() - data.low.min()) * 0.15 for y in sell_signals_y],  # Adjust the position of the text
        mode='text',
        text='Sell Signal',
        textposition='middle center',
        textfont=dict(color='red'),
        showlegend=False
    )


    candles = go.Figure(
        data=[
            go.Candlestick(
                x=data.timestamp,
                open=data.open,
                high=data.high,
                low=data.low,
                close=data.close,
                name='Heikin Ashi'
            ),
            go.Scatter(
                x=data.timestamp,
                y=data['supertrend_line'],
                name='Supertrend Line',
                mode='lines',
                line=dict(color='blue', width=1)
            ),
            buy_scatter,
            buy_text_scatter,
            sell_scatter,
            sell_text_scatter
        ]
    )
    candles.update_layout(xaxis_rangeslider_visible=False, height=400)

    indicator = px.line(x=data.timestamp, y=data.rsi, height =300)


    return candles, indicator


if __name__ == "__main__":
    app.run_server(debug=True)
