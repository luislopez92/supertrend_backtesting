import pandas as pd
from datetime import datetime
from alpaca.data.live import StockDataStream
from alpaca.data.historical import StockHistoricalDataClient
from alpaca.data.requests import StockBarsRequest
from alpaca.data.timeframe import TimeFrame
from alpaca.trading.client import TradingClient
import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import queue
import threading
import time
import atexit

# Alpaca API keys
API_KEY = 'PK9SFA1TW1EEVJ5QSAWS'
SECRET_KEY = '5g9tmNjQ1yZYUZebvjHkXef2b8JgHt0nQbO7Hg3E'

# Create a new Alpaca API object
trading_client = TradingClient(API_KEY, SECRET_KEY)

clock = trading_client.get_clock()

live_data_queue = queue.Queue()

async def quote_data_handler(data):
    # quote data will arrive here
    live_data_queue.put(data)
    print(data)

def get_bars():
    client = StockHistoricalDataClient(API_KEY, SECRET_KEY)
    request_params = StockBarsRequest(
        symbol_or_symbols=["TSLA"],
        timeframe=TimeFrame.Minute,
        start=datetime(2023, 4,1),
        end=datetime(2023, 4, 11)
    )
    bars = client.get_stock_bars(request_params)

    return bars.df

def close_wss_connection():
    wss_client.close()

if clock.is_open:
    wss_client = StockDataStream(API_KEY, SECRET_KEY)
    wss_client.subscribe_bars(quote_data_handler, 'TSLA')
    wss_thread = threading.Thread(target=wss_client.run)
    wss_thread.start()
    atexit.register(close_wss_connection)

    app = dash.Dash(__name__)

    app.layout = html.Div([
        dcc.Graph(id='live-graph', animate=True),
        dcc.Interval(
            id='interval-component',
            interval=60 * 1000,  # Update every minute
            n_intervals=0
        )
    ])

    @app.callback(Output('live-graph', 'figure'),
                  [Input('interval-component', 'n_intervals')])
    def update_graph_live(n):
        live_data = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close'])

        while not live_data_queue.empty():
            bar = live_data_queue.get()
            live_data = live_data.append({
                'timestamp': pd.to_datetime(bar.timestamp),
                'open': bar.open,
                'high': bar.high,
                'low': bar.low,
                'close': bar.close
            }, ignore_index=True)

        data = {
            'data': [
                go.Candlestick(
                    x=live_data['timestamp'],
                    open=live_data['open'],
                    high=live_data['high'],
                    low=live_data['low'],
                    close=live_data['close'],
                    name='Live Data'
                )
            ],
            'layout': go.Layout(
                title='Live Data',
                xaxis=dict(title='Time'),
                yaxis=dict(title='Price')
            )
        }

        return data

    if __name__ == '__main__':
        app.run_server(debug=True)
else:
    historical_data = get_bars()
    historical_data.reset_index(inplace=True)  # Reset the index
    #print(get_bars())

    app = dash.Dash(__name__)

    app.layout = html.Div([
        dcc.Graph(
            id='historical-graph',
            figure={
                'data': [
                    go.Candlestick(
                        x=historical_data.index,
                        open=historical_data['open'],
                        high=historical_data['high'],
                        low=historical_data['low'],
                        close=historical_data['close'],
                        name='Historical Data'
                    )
                ],
                'layout': go.Layout(
                    title='Historical Data',
                    xaxis=dict(
                        title='Time',
                        range=[datetime(2023, 4, 1), datetime(2023, 4, 11)]  # Set the x-axis range
                    ),
                    yaxis=dict(title='Price')
                )
            }
        )
    ])

    if __name__ == '__main__':
        app.run_server(debug=True)