import pandas as pd
import numpy as np
from datetime import datetime
import yfinance as yf
import math
import matplotlib.pyplot as plt
from supertrend_indicator import Supertrend
from backtest import backtest_supertrend
from import_data import import_data

# df = import_data('TSLA', '2023-03-01','30m')


# atr_period = 14
# atr_multiplier = 1.0

# supertrend = Supertrend(df, atr_period, atr_multiplier)
# df = df.join(supertrend)

def find_optimal_parameter(df):
    # predefine several parameter sets
    atr_period = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    atr_multiplier = [1.0, 1.2, 1.4,1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]
    roi_list = []
    
    # for each period and multiplier, perform backtest
    for period, multiplier in [(x,y) for x in atr_period for y in atr_multiplier]:
        new_df = df
        supertrend = Supertrend(df, period, multiplier)
        new_df = df.join(supertrend)
        new_df = new_df[period:]
        entry, exit, equity, roi, investment = backtest_supertrend(new_df, 5000, take_profit=0.20, stop_loss=0.10)
        roi_list.append((period, multiplier, roi))
    
    print(pd.DataFrame(roi_list, columns=['ATR_period','Multiplier','ROI']))
    
    # return the best parameter set
    return max(roi_list, key=lambda x:x[2])



# df = yf.download('TSLA', start='2023-02-08', end='2023-04-07',interval='30m')
# optimal_param = find_optimal_parameter(df)
# print(f'Best parameter set: ATR Period={optimal_param[0]}, Multiplier={optimal_param[1]}, ROI={optimal_param[2]}')