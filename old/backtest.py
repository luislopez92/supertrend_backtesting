import pandas as pd
import numpy as np
from datetime import datetime
import yfinance as yf
import math
import matplotlib.pyplot as plt
from supertrend_indicator import Supertrend


def backtest_supertrend(df, investment, take_profit, stop_loss):
    is_uptrend = df['Supertrend']
    close = df['Close']

    in_position = False
    equity = investment
    commission = 0.1
    share = 0
    entry = []
    exit = []
    entry_price = 0

    for i in range(2, len(df)):
        if not in_position and is_uptrend[i]:
            share = math.floor(equity / close[i] / 100) * 100
            equity -= share * close[i]
            entry.append((i, close[i]))
            entry_price = close[i]
            in_position = True

        elif in_position and not is_uptrend[i]:
            equity += share * close[i] - commission
            exit.append((i, close[i]))
            in_position = False

        if in_position:
            # Take profit
            if close[i] >= entry_price * (1 + take_profit):
                equity += share * close[i] - commission
                exit.append((i, close[i]))
                in_position = False

            # Stop loss
            if close[i] <= entry_price * (1 - stop_loss):
                equity += share * close[i] - commission
                exit.append((i, close[i]))
                in_position = False

    if in_position:
        equity += share * close[i] - commission

    earning = equity - investment

    initial_value = investment
    final_value = equity
    #roi = round(earning/investment*100,2)
    roi= round(((final_value-initial_value)/(initial_value))*100,2)

    return entry, exit, equity, roi, investment


################## TEST #####################
# atr_period = 14
# atr_multiplier = 1.0

# symbol = 'TSLA'
# df = yf.download(symbol, start='2023-02-08',interval='30m')
# supertrend = Supertrend(df, atr_period, atr_multiplier)
# df = df.join(supertrend)

# entry, exit, equity, roi, investment = backtest_supertrend(df, 5000, take_profit=0.20, stop_loss=0.10)

# print('Initial Value is: %.2f' % investment)
# print('Final Value is: %.2f' % equity)
# print('RoI is: %.2f' % roi)


# visualization (for backtest)

# plt.figure(figsize=(16,9),dpi=360)
# plt.plot(df['Close'], label='Close Price')
# plt.plot(df['Final Lowerband'],'g', label = 'Final Lowerband')
# plt.plot(df['Final Upperband'], 'r', label = 'Final Upperband')
# for e in entry:
#     plt.plot(df.index[e[0]], e[1], marker = '^', color = 'green', markersize = 2, linewidth = 0, label = 'Entry')
# for e in exit:
#     plt.plot(df.index[e[0]], e[1], marker = 'v', color = 'red', markersize = 2, linewidth = 0, label = 'Exit')
# plt.show()    