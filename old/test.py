import os
import requests
import asyncio
import json
import pandas as pd
from datetime import datetime
from websocket import WebSocketApp
from websocket import create_connection
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import numpy as np



API_KEY = 'PKGEDKBARA5OP44D2CPY'
SECRET_KEY = 'shVmYWCIgXrY51H7Wxgrj9vQhDAlJsujHFzLR1IC'
BASE_URL = 'https://paper-api.alpaca.markets'
BARS_URL = 'https://data.alpaca.markets/'  # Update this line

HEADERS = {
    'APCA-API-KEY-ID': API_KEY,
    'APCA-API-SECRET-KEY': SECRET_KEY
}

start = '2023-04-01T09:30:00-05:00'
end = '2023-04-07T16:00:00-05:00'
timeframe = '1min'
symbol = 'TSLA'


def get_historical_data(symbol, timeframe, start_date, end_date):
    url = f'{BARS_URL}/v2/stocks/{symbol}/bars?timeframe={timeframe}&start={start_date}&end={end_date}'
    response = requests.get(url, headers=HEADERS)
    print("Raw response:", response.text)  # Add this line for debugging
    response_data = response.json()
    print("Historical data response:", response_data)
    return response_data['bars']

#get_historical_data(symbol, timeframe, start, end)

def heikin_ashi(data):
    ha_data = data.copy()
    ha_data['ha_open'] = (data['o'].shift(1) + data['c'].shift(1)) / 2
    ha_data['ha_open'].iloc[0] = (data['o'].iloc[0] + data['c'].iloc[0]) / 2
    ha_data['ha_high'] = data[['h', 'o', 'c']].max(axis=1)
    ha_data['ha_low'] = data[['l', 'o', 'c']].min(axis=1)
    ha_data['ha_close'] = (data['o'] + data['h'] + data['l'] + data['c']) / 4
    return ha_data



symbol = 'TSLA'
timeframe = '1Min'
target_percent = 0.5
bars = []

# Function to handle the streaming data
def on_message(ws, message):
    global initial_supertrend
    message_data = json.loads(message)
    if 'data' in message_data and message_data['data']['ev'] == 'AM':
        bar = message_data['data']
        on_bar(ws, bar)


def supertrend(data, period, multiplier):
    data = data.copy()
    hl2 = (data['h'] + data['l']) / 2
    atr = data['h'].rolling(window=period).max() - data['l'].rolling(window=period).min()
    basic_upperband = hl2 + multiplier * atr
    basic_lowerband = hl2 - multiplier * atr
    
    final_upperband = basic_upperband.copy()
    final_lowerband = basic_lowerband.copy()
    
    for i in range(1, len(data)):
        if basic_upperband[i] < final_upperband[i - 1] or data['c'][i - 1] > final_upperband[i - 1]:
            final_upperband.loc[i] = basic_upperband[i]
        else:
            final_upperband.loc[i] = final_upperband[i - 1]
            
        if basic_lowerband[i] > final_lowerband[i - 1] or data['c'][i - 1] < final_lowerband[i - 1]:
            final_lowerband.loc[i] = basic_lowerband[i]
        else:
            final_lowerband.loc[i] = final_lowerband[i - 1]
            
    supertrend_line = pd.Series(index=data.index)
    
    for i in range(len(data)):
        if i == 0:
            supertrend_line.loc[i] = float('nan')
        elif supertrend_line[i - 1] == final_upperband[i - 1] and data['c'][i] <= final_upperband[i]:
            supertrend_line.loc[i] = final_upperband[i]
        elif supertrend_line[i - 1] == final_upperband[i - 1] and data['c'][i] > final_upperband[i]:
            supertrend_line.loc[i] = final_lowerband[i]
        elif supertrend_line[i - 1] == final_lowerband[i - 1] and data['c'][i] >= final_lowerband[i]:
            supertrend_line.loc[i] = final_lowerband[i]
        elif supertrend_line[i - 1] == final_lowerband[i - 1] and data['c'][i] < final_lowerband[i]:
            supertrend_line.loc[i] = final_upperband[i]
            
    return supertrend_line

    





def get_account():
    url = f'{BASE_URL}/v2/account'
    response = requests.get(url, headers=HEADERS)
    return response.json()

def list_positions():
    url = f'{BASE_URL}/v2/positions'
    response = requests.get(url, headers=HEADERS)
    return response.json()

def place_order(symbol, qty, side, order_type, time_in_force):
    url = f'{BASE_URL}/v2/orders'
    data = {
        'symbol': symbol,
        'qty': qty,
        'side': side,
        'type': order_type,
        'time_in_force': time_in_force
    }
    response = requests.post(url, json=data, headers=HEADERS)
    return response.json()

def on_bar(conn, bar):
    global bars
    global fig
    global initial_supertrend
    print('New bar:', bar)

    bar['t'] = datetime.utcfromtimestamp(bar['t']).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    bars.append(bar)

    if len(bars) >= 2:
        data = pd.DataFrame(bars, columns=['t', 'o', 'h', 'l', 'c', 'v'])
        data.set_index('t', inplace=True)
        data.index = pd.to_datetime(data.index)
        if not data.empty:
            heikin_ashi_data = heikin_ashi(data)
        trade_logic(heikin_ashi_data)

        # Calculate the supertrend line for the updated data
        supertrend_line = supertrend(heikin_ashi_data, 7, 3)

        # Update the chart with the new data
        update_plotly_chart(fig, heikin_ashi_data, supertrend_line)
        fig.show(renderer='browser', validate=False)



def trade_logic(data):
    super_trend_line = supertrend(data, 7, 3)
    account = get_account()
    buying_power = float(account['buying_power'])

    if data['c'][-1] > super_trend_line[-1] and data['c'][-2] <= super_trend_line[-2]:
        # Buy signal
        target_quantity = int((buying_power * target_percent) / data['c'][-1])
        if target_quantity > 0:
            place_order(
                symbol=symbol,
                qty=target_quantity,
                side='buy',
                type='market',
                time_in_force='gtc'
            )
            print("Buy order submitted")
    elif data['c'][-1] < super_trend_line[-1] and data['c'][-2] >= super_trend_line[-2]:
        # Sell signal
        positions = list_positions()
        for position in positions:
            if position['symbol'] == symbol:
                place_order(
                    symbol=symbol,
                    qty=position['qty'],
                    side='sell',
                    type='market',
                    time_in_force='gtc'
                )
                print("Sell order submitted")

def on_open(ws):
    print("WebSocket connection opened.")
    ws.send(json.dumps({
        "action": "authenticate",
        "data": {"key_id": API_KEY, "secret_key": SECRET_KEY}
    }))
    ws.send(json.dumps({
        "action": "listen",
        "data": {"streams": [f"AM.{symbol}"]}
    }))

def on_close(ws, close_status_code, close_msg):
    print(f"WebSocket connection closed with status {close_status_code}: {close_msg}")

def on_message(ws, message):
    message_data = json.loads(message)
    if 'data' in message_data and message_data['data']['ev'] == 'AM':
        bar = message_data['data']
        on_bar(ws, bar, initial_supertrend)  # Pass ws and initial_supertrend

# Initialize Plotly chart
def init_plotly_chart():
    fig = make_subplots(rows=1, cols=1, shared_xaxes=True, specs=[[{"secondary_y": False}]])
    fig.update_layout(
        title="Live Heikin-Ashi Chart with Supertrend",
        xaxis_title="Time",
        yaxis_title="Price",
        showlegend=True
    )
    
    fig.add_trace(go.Candlestick(
        x=[],
        open=[],
        high=[],
        low=[],
        close=[],
        name="Heikin-Ashi",
        increasing_line_color='green',
        decreasing_line_color='red'
    ), row=1, col=1)

    fig.add_trace(go.Scatter(
        x=[],
        y=[],
        name="Supertrend",
        line=dict(color='blue')
    ), row=1, col=1)

    return fig


# Update Plotly chart with new data
def update_plotly_chart(fig, data, supertrend):
    ha_trace = go.Candlestick(
        x=data.index,
        open=data['ha_open'],
        high=data['ha_high'],
        low=data['ha_low'],
        close=data['ha_close'],
        name="Heikin-Ashi",
        increasing_line_color='green',
        decreasing_line_color='red'
    )
    
    supertrend_trace = go.Scatter(
        x=data.index,
        y=supertrend,
        name="Supertrend",
        line=dict(color='blue')
    )
    
    if len(fig.data) == 0:
        fig.add_trace(ha_trace, row=1, col=1)
        fig.add_trace(supertrend_trace, row=1, col=1)
    else:
        fig.data[0].x = ha_trace.x
        fig.data[0].open = ha_trace.open
        fig.data[0].high = ha_trace.high
        fig.data[0].low = ha_trace.low
        fig.data[0].close = ha_trace.close

        fig.data[1].x = supertrend_trace.x
        fig.data[1].y = supertrend_trace.y


initial_supertrend = None

def main():
    global initial_supertrend
    historical_bars = get_historical_data(symbol, timeframe, start, end)

    # Calculate the initial supertrend based on historical data
    historical_data = pd.DataFrame(historical_bars)
    initial_supertrend = supertrend(historical_data, 7, 3)

    # WebSocket-related functions (on_open, on_close, etc.) should already be defined.

    # Modify the on_message function to call on_bar
    def on_message(ws, message):
        message_data = json.loads(message)
        if 'data' in message_data and message_data['data']['ev'] == 'AM':
            bar = message_data['data']
            on_bar(ws, bar, initial_supertrend)  # Pass ws and initial_supertrend

    ws = WebSocketApp('wss://stream.data.alpaca.markets/v2/iex',
                      on_message=on_message,
                      on_open=on_open,
                      on_close=on_close,
                      header=HEADERS)

    # Initialize the Plotly chart
    fig = init_plotly_chart()

    # Calculate and plot the historical Heikin-Ashi data
    historical_data.set_index('t', inplace=True)
    historical_data.index = pd.to_datetime(historical_data.index)
    heikin_ashi_data = heikin_ashi(historical_data)
    update_plotly_chart(fig, heikin_ashi_data, initial_supertrend)
    fig.show(renderer='browser', validate=False)

    ws.run_forever()


if __name__ == '__main__':
    main()


