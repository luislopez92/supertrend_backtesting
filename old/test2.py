import os
import requests
import asyncio
import json
import pandas as pd
from datetime import datetime
from websocket import WebSocketApp
from websocket import create_connection
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import numpy as np
import mplfinance as mpf




API_KEY = 'PKGEDKBARA5OP44D2CPY'
SECRET_KEY = 'shVmYWCIgXrY51H7Wxgrj9vQhDAlJsujHFzLR1IC'
BASE_URL = 'https://paper-api.alpaca.markets'
BARS_URL = 'https://data.alpaca.markets/'  # Update this line

HEADERS = {
    'APCA-API-KEY-ID': API_KEY,
    'APCA-API-SECRET-KEY': SECRET_KEY
}

start = '2023-03-01T09:30:00-05:00'
end = '2023-03-31T16:00:00-05:00'
timeframe = '59Min'
symbol = 'TSLA'


def get_historical_data(symbol, timeframe, start_date, end_date):
    url = f'{BARS_URL}/v2/stocks/{symbol}/bars?timeframe={timeframe}&start={start_date}&end={end_date}'
    response = requests.get(url, headers=HEADERS)
    #print("Raw response:", response.text)  # Add this line for debugging
    response_data = response.json()
    #print("Historical data response:", response_data)
    return response_data['bars']

#get_historical_data(symbol, timeframe, start, end)

def heikin_ashi(data):
    ha_data = data.copy()
    ha_data['ha_open'] = (data['o'].shift(1) + data['c'].shift(1)) / 2
    ha_data.at[ha_data.index[0], 'ha_open'] = (data.iloc[0]['o'] + data.iloc[0]['c']) / 2
    ha_data['ha_high'] = data[['h', 'o', 'c']].max(axis=1)
    ha_data['ha_low'] = data[['l', 'o', 'c']].min(axis=1)
    ha_data['ha_close'] = (data['o'] + data['h'] + data['l'] + data['c']) / 4
    return ha_data




symbol = 'TSLA'
timeframe = '30Min'
target_percent = 0.5
bars = []

# Function to handle the streaming data
def on_message(ws, message):
    global initial_supertrend
    message_data = json.loads(message)
    if 'data' in message_data and message_data['data']['ev'] == 'AM':
        bar = message_data['data']
        on_bar(ws, bar)


# def supertrend(data, period, multiplier):
#     data = data.copy()
#     hl2 = (data['h'] + data['l']) / 2
#     tr1 = data['h'] - data['l']
#     tr2 = (data['h'] - data['c'].shift(1)).abs()
#     tr3 = (data['l'] - data['c'].shift(1)).abs()
#     tr = pd.concat([tr1, tr2, tr3], axis=1).max(axis=1)
#     atr = tr.rolling(window=period).mean()
#     basic_upperband = hl2 + multiplier * atr
#     basic_lowerband = hl2 - multiplier * atr

#     final_upperband = basic_upperband.copy()
#     final_lowerband = basic_lowerband.copy()

#     for i in range(1, len(data)):
#         if basic_upperband[i] < final_upperband[i - 1] or data['c'][i - 1] > final_upperband[i - 1]:
#             final_upperband.loc[i] = basic_upperband[i]
#         else:
#             final_upperband.loc[i] = final_upperband[i - 1]

#         if basic_lowerband[i] > final_lowerband[i - 1] or data['c'][i - 1] < final_lowerband[i - 1]:
#             final_lowerband.loc[i] = basic_lowerband[i]
#         else:
#             final_lowerband.loc[i] = final_lowerband[i - 1]

#     supertrend_line = pd.Series('nan', index=data.index, dtype='float64')

#     for i in range(len(data)):
#         if i == 0:
#             supertrend_line.loc[i] = float('nan')
#         elif supertrend_line.iloc[i - 1] == final_upperband.iloc[i - 1] and data['c'].iloc[i] <= final_upperband.iloc[i]:
#             supertrend_line.loc[i] = final_upperband[i]
#         elif supertrend_line.iloc[i - 1] == final_upperband.iloc[i - 1] and data['c'].iloc[i] > final_upperband.iloc[i]:
#             supertrend_line.loc[i] = final_lowerband[i]
#         elif supertrend_line.iloc[i - 1] == final_lowerband.iloc[i - 1] and data['c'].iloc[i] >= final_lowerband.iloc[i]:
#             supertrend_line.loc[i] = final_lowerband[i]
#         elif supertrend_line.iloc[i - 1] == final_lowerband.iloc[i - 1] and data['c'].iloc[i] < final_lowerband.iloc[i]:
#             supertrend_line.loc[i] = final_upperband[i]

#     # Add this line to update the first value of supertrend_line
#     supertrend_line.loc[supertrend_line.index[0]] = final_upperband.loc[final_upperband.index[0]] if data['c'].iloc[0] > final_upperband.iloc[0] else final_lowerband.loc[final_lowerband.index[0]]


#     return supertrend_line

def supertrend(data, period, multiplier):
    data = data.copy()
    hl2 = (data['h'] + data['l']) / 2
    tr1 = data['h'] - data['l']
    tr2 = (data['h'] - data['c'].shift(1)).abs()
    tr3 = (data['l'] - data['c'].shift(1)).abs()
    tr = pd.concat([tr1, tr2, tr3], axis=1).max(axis=1)
    atr = tr.rolling(window=period).mean()
    basic_upperband = hl2 + multiplier * atr
    basic_lowerband = hl2 - multiplier * atr

    final_upperband = basic_upperband.copy()
    final_lowerband = basic_lowerband.copy()

    for i in range(1, len(data)):
        if basic_upperband[i] < final_upperband[i - 1] or data['c'][i - 1] > final_upperband[i - 1]:
            final_upperband.loc[i] = basic_upperband[i]
        else:
            final_upperband.loc[i] = final_upperband[i - 1]

        if basic_lowerband[i] > final_lowerband[i - 1] or data['c'][i - 1] < final_lowerband[i - 1]:
            final_lowerband.loc[i] = basic_lowerband[i]
        else:
            final_lowerband.loc[i] = final_lowerband[i - 1]

    supertrend_line = pd.Series(np.nan, index=data.index, dtype='float64')
    supertrend_df = pd.DataFrame(index=data.index, columns=['supertrend'])
    for i in range(len(data)):
        if i == 0:
            supertrend_df.iloc[i] = float('nan')
        elif supertrend_df.iloc[i - 1]['supertrend'] == final_upperband.iloc[i - 1] and data['c'].iloc[i] <= final_upperband.iloc[i]:
            supertrend_df.iloc[i] = final_upperband[i]
        elif supertrend_df.iloc[i - 1]['supertrend'] == final_upperband.iloc[i - 1] and data['c'].iloc[i] > final_upperband.iloc[i]:
            supertrend_df.iloc[i] = final_lowerband[i]
        elif supertrend_df.iloc[i - 1]['supertrend'] == final_lowerband.iloc[i - 1] and data['c'].iloc[i] >= final_lowerband.iloc[i]:
            supertrend_df.iloc[i] = final_lowerband[i]
        elif supertrend_df.iloc[i - 1]['supertrend'] == final_lowerband.iloc[i - 1] and data['c'].iloc[i] < final_lowerband.iloc[i]:
            supertrend_df.iloc[i] = final_upperband[i]
    supertrend_line = supertrend_df

    return supertrend_line



# Add this function to your code
def convert_to_dataframe(data):
    df = pd.DataFrame(data)
    df['t'] = pd.to_datetime(df['t'])
    df.set_index('t', inplace=True)
    return df

# Modify the following lines in your code:
data = get_historical_data(symbol, timeframe, start, end)
data_df = convert_to_dataframe(data)  # Add this line to convert data to DataFrame


test1 = supertrend(data_df,1,1)

# Combine data_df and supertrend_df into a single DataFrame
combined_df = pd.concat([data_df, test1], axis=1)

print(combined_df)

    

# def get_account():
#     url = f'{BASE_URL}/v2/account'
#     response = requests.get(url, headers=HEADERS)
#     return response.json()

# def list_positions():
#     url = f'{BASE_URL}/v2/positions'
#     response = requests.get(url, headers=HEADERS)
#     return response.json()

# def place_order(symbol, qty, side, order_type, time_in_force):
#     url = f'{BASE_URL}/v2/orders'
#     data = {
#         'symbol': symbol,
#         'qty': qty,
#         'side': side,
#         'type': order_type,
#         'time_in_force': time_in_force
#     }
#     response = requests.post(url, json=data, headers=HEADERS)
#     return response.json()

# def on_bar(conn, bar):
#     global bars
#     global fig
#     global initial_supertrend
#     print('New bar:', bar)

#     bar['t'] = datetime.utcfromtimestamp(bar['t']).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
#     bars.append(bar)

#     if len(bars) >= 2:
#         data = pd.DataFrame(bars, columns=['t', 'o', 'h', 'l', 'c', 'v'])
#         data.set_index('t', inplace=True)
#         data.index = pd.to_datetime(data.index)
#         heikin_ashi_data = heikin_ashi(data)
#         trade_logic(heikin_ashi_data)

#         # Calculate the supertrend line for the updated data
#         supertrend_line = supertrend(heikin_ashi_data, 7, 3)

#         # Update the chart with the new data
#         update_plotly_chart(fig, heikin_ashi_data, supertrend_line)
#         fig.show(renderer='browser', validate=False)



# def trade_logic(data):
#     super_trend_line = supertrend(data, 7, 3)
#     account = get_account()
#     buying_power = float(account['buying_power'])

#     if data['c'][-1] > super_trend_line[-1] and data['c'][-2] <= super_trend_line[-2]:
#         # Buy signal
#         target_quantity = int((buying_power * target_percent) / data['c'][-1])
#         if target_quantity > 0:
#             place_order(
#                 symbol=symbol,
#                 qty=target_quantity,
#                 side='buy',
#                 type='market',
#                 time_in_force='gtc'
#             )
#             print("Buy order submitted")
#     elif data['c'][-1] < super_trend_line[-1] and data['c'][-2] >= super_trend_line[-2]:
#         # Sell signal
#         positions = list_positions()
#         for position in positions:
#             if position['symbol'] == symbol:
#                 place_order(
#                     symbol=symbol,
#                     qty=position['qty'],
#                     side='sell',
#                     type='market',
#                     time_in_force='gtc'
#                 )
#                 print("Sell order submitted")

# def on_open(ws):
#     print("WebSocket connection opened.")
#     ws.send(json.dumps({
#         "action": "authenticate",
#         "data": {"key_id": API_KEY, "secret_key": SECRET_KEY}
#     }))
#     ws.send(json.dumps({
#         "action": "listen",
#         "data": {"streams": [f"AM.{symbol}"]}
#     }))

# def on_close(ws, close_status_code, close_msg):
#     print(f"WebSocket connection closed with status {close_status_code}: {close_msg}")

# def on_message(ws, message):
#     message_data = json.loads(message)
#     if 'data' in message_data and message_data['data']['ev'] == 'AM':
#         bar = message_data['data']
#         on_bar(ws, bar, initial_supertrend)  # Pass ws and initial_supertrend

# # Initialize mplfinance chart
# def init_mplfinance_chart(heikin_ashi_data, initial_supertrend):
#     plot_data = heikin_ashi_data.copy()
#     plot_data['supertrend'] = initial_supertrend
#     ap = [mpf.make_addplot(plot_data['supertrend'], panel=0, color='b', secondary_y=False, linestyle='-')]
#     mpf.plot(plot_data, type='candle', style='yahoo', title='Supertrend Backtesting', ylabel='Price', addplot=ap)





# def main():
#     global initial_supertrend
#     historical_bars = get_historical_data(symbol, timeframe, start, end)

#     # Calculate the initial supertrend based on historical data
#     historical_data = pd.DataFrame(historical_bars)
#     initial_supertrend = supertrend(historical_data, 7, 3)

#     # WebSocket-related functions (on_open, on_close, etc.) should already be defined.

#     # Modify the on_message function to call on_bar
#     def on_message(ws, message):
#         message_data = json.loads(message)
#         if 'data' in message_data and message_data['data']['ev'] == 'AM':
#             bar = message_data['data']
#             on_bar(ws, bar, initial_supertrend)  # Pass ws and initial_supertrend

#     ws = WebSocketApp('wss://stream.data.alpaca.markets/v2/iex',
#                       on_message=on_message,
#                       on_open=on_open,
#                       on_close=on_close,
#                       header=HEADERS)

#     # Calculate and plot the historical Heikin-Ashi data
#     historical_data.set_index('t', inplace=True)
#     historical_data.index = pd.to_datetime(historical_data.index)
#     heikin_ashi_data = heikin_ashi(historical_data)
#     initial_supertrend = supertrend(historical_data, 7, 3)
#     #heikin_ashi_data['supertrend'] = initial_supertrend
#     print(initial_supertrend)

#     init_mplfinance_chart(heikin_ashi_data, initial_supertrend)

#     ws.run_forever()


# if __name__ == '__main__':
#     main()


