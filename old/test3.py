import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from datetime import datetime
from alpaca.data.live import StockDataStream
from alpaca.data.historical import StockHistoricalDataClient
from alpaca.data.requests import StockBarsRequest
from alpaca.data.timeframe import TimeFrame
from datetime import datetime
from alpaca.trading.client import TradingClient

# Alpaca API keys
API_KEY = 'PK9SFA1TW1EEVJ5QSAWS'
SECRET_KEY = '5g9tmNjQ1yZYUZebvjHkXef2b8JgHt0nQbO7Hg3E'

# Create a new Alpaca API object
trading_client = TradingClient(API_KEY, SECRET_KEY)

clock = trading_client.get_clock()

client = StockHistoricalDataClient(API_KEY,SECRET_KEY)

request_params = StockBarsRequest(
                        symbol_or_symbols=["TSLA"],
                        timeframe=TimeFrame.Minute,
                        start=datetime(2023, 4, 10),
                        end=datetime(2023, 4, 11)
                 )

def get_bars():
    request_params = StockBarsRequest(
                        symbol_or_symbols=["TSLA"],
                        timeframe=TimeFrame.Minute,
                        start=datetime(2023, 4, 10),
                        end=datetime(2023, 4, 11)
                 )
    client.get_stock_bars(request_params)

    bars = client.get_stock_bars(request_params)

    return bars.df

test = get_bars()

print(test)

# Define the maximum number of bars to plot on the chart
max_bars = 100

# Define a function to retrieve and plot the bar data
def plot_bars(symbol, bars):
    # Convert the bar data to a pandas DataFrame and plot it
    df = pd.DataFrame(columns=['open', 'high', 'low', 'close', 'volume', 'time'])
    for bar in bars:
        df = df.append({
            'open': bar.open,
            'high': bar.high,
            'low': bar.low,
            'close': bar.close,
            'volume': bar.volume,
            'time': bar.timestamp
        }, ignore_index=True)
    df['time'] = pd.to_datetime(df['time'], unit='s')
    plt.clf()
    plt.plot(df['time'], df['close'])
    plt.xlabel('Time')
    plt.ylabel('Price')
    plt.title(symbol)

# Define a function to be called every time new data is received
def update_plot(data):
    # Extract the symbol and price from the data
    symbol = data.symbol
    price = data.close

    # Print the symbol and price to the console
    print(f'{symbol}: {price:.2f}')

    # Get the historical bar data from Alpaca
    barset = get_bars()
    bars = barset

    # Plot the historical and live bar data
    plot_bars(bars)


# Check if the market is currently open
if clock.is_open:
    # The market is open

    # Subscribe to the 'TSLA' quote data stream
    wss_client = StockDataStream(API_KEY, SECRET_KEY)
    wss_client.subscribe_quotes(update_plot, 'TSLA')
    wss_client.run()

    # Start the animation to continuously update the plot
    ani = FuncAnimation(plt.gcf(), lambda _: None, interval=60*1000)
else:
    # The market is closed

    # Get the historical bar data from Alpaca
    barset = get_bars()
    bars = barset

    # Plot the historical bar data
    plot_bars('TSLA',bars)

# Show the plot
plt.show()
