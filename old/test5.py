from dash import Dash, dcc, html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go

import pandas as pd
import numpy as np
import pandas_ta as ta
import requests
from datetime import datetime
from alpaca.data.live import StockDataStream
import threading

app = Dash(__name__)

# Alpaca API keys
API_KEY ='PKA1IYK7LZC2ZLVXD07H'
SECRET_KEY = 'IRle393smy6mSm9WXFaeIpik5JPTEtDtYkXf68dt'

# Initialize an empty DataFrame with the desired columns
live_data = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])

async def quote_data_handler(data):
    global live_data
    # Append the new data to the DataFrame
    live_data = live_data.append({
        'timestamp': data.timestamp.replace(tzinfo=None),
        'open': data.open,
        'high': data.high,
        'low': data.low,
        'close': data.close,
        'volume': data.volume
    }, ignore_index=True)
    print(live_data.tail(1))  # Print the latest row

    return live_data



def run_wss_client():
    wss_client = StockDataStream(API_KEY, SECRET_KEY)
    wss_client.subscribe_bars(quote_data_handler, 'TSLA')
    wss_client.run()

app.layout = html.Div([

    html.H1(id="count-up"),

    dcc.Graph(id="candles"),

    dcc.Graph(id="indicator"),

    dcc.Interval(id="interval",interval=2000), 

    ])

@app.callback(
    Output("candles","figure"),
    Output("indicator","figure"),
    Input("interval", "n_intervals"),
    )

def update_figure(ohlc,n_intervals):

    #global live_data
    live_data = ohlc

    data = live_data.copy()

    data["rsi"] = ta.rsi(data.close.astype(float))

    candles =  go.Figure(
                        data =  [
                            go.Candlestick(
                                x = data.timestamp,
                                open = data.open,
                                high = data.high,
                                low = data.low,
                                close = data.close 
                                )])

    candles.update_layout(xaxis_rangeslider_visible=False, height=400)

    indicator = px.line(x=data.timestamp, y=data.rsi, height =300)
                        

    return candles, indicator


if __name__ == '__main__':
        # Start the WebSocket client in a separate thread
    wss_thread = threading.Thread(target=run_wss_client)
    wss_thread.start()

    app.run_server(debug=True)