import os
import alpaca_trade_api as tradeapi
import websocket
import json
import pandas as pd
import numpy as np
from supertrend_indicator import Supertrend

ALPACA_API_KEY = os.getenv('ALPACA_API_KEY')
ALPACA_SECRET_KEY = os.getenv('ALPACA_SECRET_KEY')
ALPACA_BASE_URL = 'https://paper-api.alpaca.markets'

api = tradeapi.REST(ALPACA_API_KEY, ALPACA_SECRET_KEY, ALPACA_BASE_URL, api_version='v2')

symbol = 'TSLA'
timeframe = 'minute'
bar_count = 50
atr_multiplier = 1

ws = None
df = None
is_position_open = False

def on_message(ws, message):
    global df, is_position_open
    msg = json.loads(message)
    if 'data' in msg:
        bar = msg['data']
        new_row = pd.DataFrame({
            'close': [bar['c']],
            'high': [bar['h']],
            'low': [bar['l']],
            'open': [bar['o']],
            'volume': [bar['v']],
            'timestamp': pd.to_datetime([bar['t']])
        }).set_index('timestamp')

        df = pd.concat([df, new_row])
        df = supertrend(df, bar_count, atr_multiplier)
        latest_row = df.iloc[-1]

        if not is_position_open and latest_row['supertrend_signal'] == 1:
            print('Buy signal')
            api.submit_order(
                symbol=symbol,
                qty=1,
                side='buy',
                type='market',
                time_in_force='gtc'
            )
            is_position_open = True
        elif is_position_open and latest_row['supertrend_signal'] == -1:
            print('Sell signal')
            api.submit_order(
                symbol=symbol,
                qty=1,
                side='sell',
                type='market',
                time_in_force='gtc'
            )
            is_position_open = False

def main():
    global ws, df
    df = api.get_barset(symbol, timeframe, limit=bar_count).df[symbol]
    df = supertrend(df, bar_count, atr_multiplier)

    ws = websocket.WebSocketApp(
        f"wss://stream.data.alpaca.markets/v2/iex/t/{symbol}",
        on_message=on_message
    )
    ws.run_forever()

if __name__ == '__main__':
    main()


