import pandas as pd
import numpy as np
import yfinance as yf
import math
import mplfinance as mpf
from heikin_ashi_data import heikin_ashi
from supertrend_indicator import Supertrend
from import_data import import_data

# atr_period = 14
# atr_multiplier = 2.0

# symbol = 'TSLA'
# data = yf.download(symbol, start='2023-03-01', interval='30m')
# data = data.dropna()
# df = heikin_ashi(data)
# supertrend = Supertrend(df, atr_period, atr_multiplier)
# df = df.join(supertrend)

# def plot_supertrend_ha(symbol,start,interval,atr_period,atr_multiplier):

#     atr_period = atr_period
#     atr_multiplier = atr_multiplier

#     symbol = symbol
#     data = yf.download(symbol, start=start, interval=interval)
#     data = data.dropna()
#     df = heikin_ashi(data)
#     supertrend = Supertrend(df, atr_period, atr_multiplier)
#     df = df.join(supertrend)

#     # Prepare the data for mplfinance
#     df_mpf = df.copy()
#     df_mpf.index = pd.to_datetime(df_mpf.index)
#     df_mpf = df_mpf[['Open', 'High', 'Low', 'Close']]

#     # Create a custom plot style
#     mc = mpf.make_marketcolors(up='g', down='r', inherit=True)
#     s = mpf.make_mpf_style(base_mpf_style='yahoo', marketcolors=mc)


#     apd = [
#         mpf.make_addplot(df['Final Lowerband'], color='g', secondary_y=False),
#         mpf.make_addplot(df['Final Upperband'], color='r', secondary_y=False),

#     ]

#     mpf.plot(df_mpf, type='candle', style=s, addplot=apd, volume=False, figsize=(16, 9), title='Supertrend with Entry/Exit Signals', ylabel='Price')

# #plot_supertrend_ha('TSLA','2023-03-01','30m',14,2.0)

#df = import_data('TSLA', '2023-03-01','2023-04-01' '30m')

def plot_supertrend_ha(df,atr_period,atr_multiplier):

    atr_period = atr_period
    atr_multiplier = atr_multiplier

    df = heikin_ashi(df)
    supertrend = Supertrend(df, atr_period, atr_multiplier)
    df = df.join(supertrend)

    # Prepare the data for mplfinance
    df_mpf = df.copy()
    df_mpf.index = pd.to_datetime(df_mpf.index)
    df_mpf = df_mpf[['Open', 'High', 'Low', 'Close']]

    # Create a custom plot style
    mc = mpf.make_marketcolors(up='g', down='r', inherit=True)
    s = mpf.make_mpf_style(base_mpf_style='yahoo', marketcolors=mc)


    apd = [
        mpf.make_addplot(df['Final Lowerband'], color='g', secondary_y=False),
        mpf.make_addplot(df['Final Upperband'], color='r', secondary_y=False),

    ]

    mpf.plot(df_mpf, type='candle', style=s, addplot=apd, volume=False, figsize=(16, 9), title='Supertrend with Entry/Exit Signals', ylabel='Price')

#plot_supertrend_ha(df,14,2.0)


