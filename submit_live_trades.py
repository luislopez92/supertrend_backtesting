import pandas as pd
from datetime import datetime
from alpaca.data.live import StockDataStream
import alpaca_trade_api as tradeapi
import requests
import json
import time


# Alpaca API keys
API_KEY ='PKKBLWOBXR8MGKFPDTOR'
SECRET_KEY = 'sheybuExwJj7PeUZMzfxcEgJbAyX8sgbcJmWUqF3'
HEADERS = {'APCA-API-KEY-ID': API_KEY, 'APCA-API-SECRET-KEY': SECRET_KEY}

BASE_URL = "https://paper-api.alpaca.markets"
POSITIONS_URL = "{}/v2/positions".format(BASE_URL)

# Calculate Heikin Ashi data
def heikin_ashi(data):
    ha_data = pd.DataFrame(index=data.index)
    ha_data['timestamp'] = data.timestamp #added this
    ha_data['close'] = (data.open + data.high + data.low + data.close) / 4
    ha_data['open'] = (data.open.shift() + data.close.shift()) / 2
    ha_data.open.iloc[0] = (data.open[0] + data.close[0]) / 2
    ha_data['high'] = data[['high', 'open', 'close']].max(axis=1)
    ha_data['low'] = data[['low', 'open', 'close']].min(axis=1)
    return ha_data


# Initialize an empty DataFrame with the desired columns
live_data = pd.DataFrame(columns=['timestamp', 'open', 'high', 'low', 'close', 'volume'])

# Initialize the Alpaca API connection
api = tradeapi.REST(API_KEY, SECRET_KEY, base_url='https://paper-api.alpaca.markets', api_version='v2')

def delete_positions(symbol):
	data = {
		"symbol": symbol
	}

	r = requests.delete(POSITIONS_URL+'/'+symbol, headers=HEADERS)

	return json.loads(r.content)


def supertrend(data, period, multiplier):
    data = data.copy()
    hl2 = (data['high'] + data['low']) / 2
    atr = data['high'].rolling(window=period).max() - data['low'].rolling(window=period).min()
    basic_upperband = hl2 + multiplier * atr
    basic_lowerband = hl2 - multiplier * atr

    final_upperband = basic_upperband.copy()
    final_lowerband = basic_lowerband.copy()

    for i in range(1, len(data)):
        if basic_upperband[i] < final_upperband[i - 1] or data['close'][i - 1] > final_upperband[i - 1]:
            final_upperband.loc[i] = basic_upperband[i]
        else:
            final_upperband.loc[i] = final_upperband[i - 1]

        if basic_lowerband[i] > final_lowerband[i - 1] or data['close'][i - 1] < final_lowerband[i - 1]:
            final_lowerband.loc[i] = basic_lowerband[i]
        else:
            final_lowerband.loc[i] = final_lowerband[i - 1]

    st = pd.Series(float('nan'), index=data.index)

    # Initialize the first non-NaN value
    st[period] = final_upperband[period]

    for i in range(period + 1, len(data)):
        pre_upband = final_upperband[i - 1]
        pre_lowband = final_lowerband[i - 1]

        if data['close'][i] <= final_upperband[i] and (st[i - 1] == pre_upband):
            st.loc[i] = final_upperband[i]
        elif (st[i - 1] == pre_upband) and (data['close'][i] > final_upperband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] >= final_lowerband[i]):
            st.loc[i] = final_lowerband[i]
        elif (st[i - 1] == pre_lowband) and (data['close'][i] < final_lowerband[i]):
            st.loc[i] = final_upperband[i]


    supertrend_line = st

    return final_upperband, final_lowerband, supertrend_line


async def quote_data_handler(data):
    global live_data
    # Append the new data to the DataFrame
    live_data = live_data.append({
        'timestamp': data.timestamp.replace(tzinfo=None),
        'open': data.open,
        'high': data.high,
        'low': data.low,
        'close': data.close,
        'volume': data.volume
    }, ignore_index=True)

    # Calculate Heikin Ashi data
    live_data_ha =heikin_ashi(live_data)

    
    # Calculate final_upperband and final_lowerband
    period = 1
    multiplier = 1
    final_upperband, final_lowerband, supertrend_line = supertrend(live_data_ha, period, multiplier)
    
    # Add columns to the live_data DataFrame
    live_data_ha['final_upperband'] = final_upperband
    live_data_ha['final_lowerband'] = final_lowerband
    live_data_ha['supertrend_line'] = supertrend_line

    print(live_data_ha)  # Print the latest row

    # Trade logic
    if len(live_data_ha) > period + 1:
        if live_data_ha['close'].iloc[-1] > supertrend_line.iloc[-1] and live_data_ha['close'].iloc[-2] <= supertrend_line.iloc[-2]:
            # Buy signal
            print("Buy signal")


            # Cancel all open orders
            delete_positions("TSLA")
            print('Previous Position Cancelled')

            time.sleep(1)

            latest_price = api.get_latest_trade('TSLA')

            #slippage = abs(latest_price.p-live_data_ha['close'][0])
            slippage = abs(latest_price.p-live_data_ha['close'].iloc[-1])
            
            # Submit buy order
            account = api.get_account()
            buying_power = float(account.buying_power)
            target_percent = 0.1  # Adjust this value to your desired target percentage
            target_quantity = int((buying_power * target_percent) / live_data_ha['close'].iloc[-1])
            
            if target_quantity > 0:
                if slippage < 1:
                    api.submit_order(
                        symbol='TSLA',
                        qty=target_quantity,
                        side='buy',
                        type='market',
                        time_in_force='gtc'
                    )
                    print("Buy order submitted")
                    print("slippage is: "+str(slippage))
                else:
                    print("Slippage is: "+str(slippage))
                    pass

        elif live_data_ha['close'].iloc[-1] < supertrend_line.iloc[-1] and live_data_ha['close'].iloc[-2] >= supertrend_line.iloc[-2]:
            # Sell signal
            print("Sell signal")

            # Cancel all open orders
            delete_positions("TSLA")
            print('Previous Position Cancelled')

            time.sleep(1)

            latest_price = api.get_latest_trade('TSLA')

            # slippage = abs(latest_price.p-live_data_ha['close'][0])
            slippage = abs(latest_price.p-live_data_ha['close'].iloc[-1])

            # Submit sell order
            if slippage < 1:
                positions = api.list_positions()
                for position in positions:
                    if position.symbol == 'TSLA':
                        api.submit_order(
                            symbol='TSLA',
                            qty=int(position.qty),
                            side='sell',
                            type='market',
                            time_in_force='gtc'
                        )
                        print("Sell order submitted")
                        print("slippage is: "+str(slippage))
            else:
                print("Slippage is: "+str(slippage))
                pass

    

wss_client = StockDataStream(API_KEY, SECRET_KEY)
wss_client.subscribe_bars(quote_data_handler, 'TSLA')

wss_client.run()
