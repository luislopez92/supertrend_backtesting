from bt_parameter_optimization import find_optimal_bt_parameter


def optimize_tickers(tickers, start, end, interval, investment, commission):
    results = []

    for ticker in tickers:
        print(f"Optimizing parameters for {ticker}:")
        best_params_tuple = find_optimal_bt_parameter(
            ticker, start, end, interval, investment, commission
        )
        best_params = (best_params_tuple[0], best_params_tuple[1])
        best_roi = best_params_tuple[2]
        print(
            f"Best parameters for {ticker}: ATR_period={best_params[0]}, Multiplier={best_params[1]}, ROI={best_roi}\n"
        )
        results.append((ticker, best_params, best_roi))

    results.sort(key=lambda x: x[2], reverse=True)

    for ticker, best_params, best_roi in results:
        print(
            f"{ticker}: ATR_period={best_params[0]}, Multiplier={best_params[1]}, ROI={best_roi}"
        )

    return results

#optimize_tickers(tickers, start, end, interval, investment, comission)