import pandas as pd
import time
from finvizfinance.screener.overview import Overview
from finvizfinance.quote import finvizfinance as FinvizQuote


def stock_screener():
    foverview = Overview()
    filters_dict = {'Country':'USA', 'Market Cap.': 'Mega ($200bln and more)'}
    foverview.set_filter(filters_dict=filters_dict)
    df = foverview.screener_view()

    # Fetch ATR for each stock
    def get_atr(ticker):
        time.sleep(0.1)  # Add a delay between requests
        stock_info = FinvizQuote(ticker).ticker_fundament()
        return float(stock_info['ATR'])

    df['ATR'] = df['Ticker'].apply(get_atr)

    # Sort the DataFrame by the 'ATR' column in descending order
    df_sorted = df.sort_values(by='ATR', ascending=False)

    # Print the top 5 stocks with the highest ATR
    #print(df_sorted.head(20))

    df = df_sorted

    df['Magic'] = df['Volume'] * df['ATR'] / 1000000

    # Sort the DataFrame based on the ATR column in descending order
    sorted_df = df.sort_values(by='Magic', ascending=False)

    print(sorted_df)

    # Get the "Ticker" column from the sorted DataFrame and convert it to a list
    tickers_sorted_by_magic = sorted_df['Ticker'].tolist()[:4]

    print(tickers_sorted_by_magic)

    #print(tickers_sorted_by_magic)

    return tickers_sorted_by_magic

#stock_screener()